/*
Мини-библиотека обработчик картинок

Img(source) - базовый обьект-обертка, где source - DOM-элемент img.
Методы:
Img.rotateExif() - ориентирует картинку соответственно данных exif (если такие есть), возвращает Img;
Img.rotate(arg) - где arg - число, поворачивает картинку на arg градусов, возвращает Img;
Img.resize(arg) - где arg - обьект, содержащий одно из числовых свойств min или max:
                  min - вписывает картинку в квадрат со стороной min пикселей,
                  max - описывает картинку вокруг квадрата со стороной max пикселей,
                  изменяет размер картинки, возвращает Img;
Img.cropSquare() - обрезает картинку до наибольшего квадратного размера, возвращает Img;
Img.append(arg) - если arg строка - вставляет картинку в качестве img-тега в document.getElementById(arg),
                  если arg функция - выполяется ее запуск с аргументом-событием загрузки картинки.
*/
function Img(source) {
  this.orientation = getOrientation(source);
  this.canvas = document.createElement('canvas');
  this.source = source;
  this.width = this.source.width;
  this.height = this.source.height;
  
  /* Следующие функции
     getOrientation и base64ToArrayBuffer 
     взяты с библиотеки exif.js
     https://github.com/exif-js/
     с некоторыми изменениями
  */
  function getOrientation(img) {
    var view = new DataView(base64ToArrayBuffer(img.src));
    if (view.getUint16(0, false) != 0xFFD8) return false;
    var length = view.byteLength;
    var offset = 2;
    while (offset < length) {
      var marker = view.getUint16(offset, false);
      offset += 2;
      if (marker == 0xFFE1) {
        var little = view.getUint16(offset += 8, false) == 0x4949;
        offset += view.getUint32(offset + 4, little);
        var tags = view.getUint16(offset, little);
        offset += 2;
        for (var i=0; i<tags; i++)
          if (view.getUint16(offset + (i * 12), little) == 0x0112)
            return view.getUint16(offset + (i * 12) + 8, little);
      }
      else if ((marker & 0xFF00) != 0xFF00) break;
      else offset += view.getUint16(offset, false);
    }
    return false;
  }
  function base64ToArrayBuffer(base64) {
    var binary = atob(base64.replace(/^data\:([^\;]+)\;base64,/gmi, ''));
    var len = binary.length;
    var buffer = new ArrayBuffer(len);
    var view = new Uint8Array(buffer);
    for (var i=0; i<len; i++) view[i] = binary.charCodeAt(i);
    return buffer;
  }
}
Img.prototype.rotateExif = function() {
  if (!this.orientation) return this;
  var degree;
  switch (this.orientation) {
    case 3:
      degree = 180;
      break;
    case 6:
      degree = 90;
      break;
    case 8:
      degree = 270;
      break;
    default:
      degree = 0;
      break;
  }
  return this.rotate(degree);
}
Img.prototype.rotate = function(degree) {
  var ctx = this.canvas.getContext('2d');
  if (degree) {
    if ((degree / 90) % 2 == 0) {
      this.canvas.width = this.width;
      this.canvas.height = this.height;
    } else {
      this.canvas.width = this.height;
      this.canvas.height = this.width;
    }
    ctx.translate(this.canvas.width/2, this.canvas.height/2);
    ctx.rotate(degree * Math.PI / 180);
    this.x = -this.width/2;
    this.y = -this.height/2
  }
  return this;
}
Img.prototype.resize = function(options) {
  var scale = getScale(this.width, this.height);
  if (scale) {
    resizeIt(this, scale);
    this.width = Math.floor(this.width / scale);
    this.height = Math.floor(this.height / scale);
  }
  this.canvas.width = this.width;
  this.canvas.height = this.height;
  return this;

  function getScale(width, height) {
    var ratio = width / height;
    if (options.max && options.max < width || options.max < height) {
      return (ratio > 1) ? width / options.max : height / options.max;
    }
    if (options.min && options.min < width && options.min < height) {
      return (ratio > 1) ? height / options.min : width / options.min;
    }
    return 0;
  }
  function resizeIt(self, scale) {
    self.reSource = document.createElement('canvas');
    var reCtx = self.reSource.getContext('2d');
    var reWidth = self.reSource.width = self.width / scale;
    var reHeight = self.reSource.height = self.height / scale;
    if (scale <= 2) {
      reCtx.drawImage(self.source, 0, 0, reWidth, reHeight);
      return;
    }
    var tmpCv = document.createElement('canvas');
    var tmpCtx = tmpCv.getContext('2d');
    var w = self.width;
    var h = self.height;
    var halfW = tmpCv.width = w / 2;
    var halfH = tmpCv.height = h / 2;
    var tmpImg = self.source;
    while (scale > 2) {
      tmpCtx.drawImage(tmpImg, 0, 0, Math.floor(w), Math.floor(h), 0, 0, Math.floor(halfW), Math.floor(halfH));
      tmpImg = tmpCv;
      w = halfW;
      h = halfH;
      halfW /= 2;
      halfH /= 2;
      scale = getScale(w, h);
    }
    reCtx.drawImage(tmpCv, 0, 0, Math.floor(w), Math.floor(h), 0, 0, reWidth, reHeight);
  }
}
Img.prototype.cropSquare = function() {
  this.size = Math.min(this.height, this.width);
  this.sx = (this.width - this.size) / 2;
  this.sy = (this.height - this.size) / 2;
  return this;
}
Img.prototype.append = function(param) {
  var imgTag = document.createElement('img');
  var x = this.x || 0, y = this.y || 0;
  var source = this.reSource ? this.reSource : this.source;
  this.canvas.getContext('2d').drawImage(source, x, y, this.width, this.height);
  if (this.size) {
    var squareCanvas = document.createElement('canvas');
    squareCanvas.width = squareCanvas.height = this.size;
    squareCanvas.getContext('2d').drawImage(this.canvas, this.sx, this.sy, this.size, this.size, x, y, this.size, this.size);
    imgTag.src = squareCanvas.toDataURL("image/jpeg", .85);
  } else {
    imgTag.src = this.canvas.toDataURL("image/jpeg", .85);
  }
  if (typeof param == 'string') document.getElementById(param).appendChild(imgTag);
  if (param instanceof Function) addEvent(imgTag, 'load', param);
}