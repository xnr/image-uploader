/*
загрузчик картинок
*/
function imageUploader(options) {
  var file = {
    support: function(input, callback) {
      if (!input.files) {
        info.error('Для завантаження фото оновіть браузер до останньої версії');
        callback.call(this);
        return false;
      }
      return true;
    },
    upload: function(body, url, callback) {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", url, true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {
          info.error('Помилка завантаження. ' + xhr.status + ': ' + xhr.statusText);
          return;
        } else {
          callback.call(this);
        }
      }
      xhr.send(body);
    }
  }
  if (!file.support(
        document.getElementById(options.button),
        function() {blockForm(document.forms.photo_form, document.getElementById(options.form_button))}
      )
  ) return;
  postInput(options.button, options.form_button, options.container);

function blockForm(form, button) {
  form.pic.setAttribute('disabled', true);
  button.setAttribute('disabled', true);
}
function unblockForm(form, button) {
  form.pic.removeAttribute('disabled');
  button.removeAttribute('disabled');
  button.removeAttribute('style');
}
function postInput(id, postId, container) {
  var cntnr = document.getElementById(container);
  var postButton = document.getElementById(postId);
  var links = cntnr.getElementsByTagName("a");
  var images = cntnr.getElementsByTagName("img");
  var fileInput = document.getElementById(id);
  var addPhotoStatus = (function(input) {
    var span = input.nextSibling;
    span.busy = function(busy) {
      span.textContent = busy ? "Обробка..." : "Додати фото";
    }
    return span;
  })(fileInput);
  unblockForm(document.forms.photo_form, postButton);
  addPhotoStatus.busy(false);
  addEvent(fileInput, 'change', function(e) {
    var files = getTarget(e).files;
    for (var i=0; i<files.length; i++) (function(i) {
      if (!files[i].type.match('image.*')) return;
      var aId = files[i].name.replace(/\s|\./g, '');
      if (document.getElementById(aId)) return;
      addPhotoStatus.busy(true);
      var div = document.createElement('div');
      div.className = 'photo';
      addTag('a', {id: aId}, div);
      addTag('div', {className: 'remove', title: 'видалити фото'}, div);
      addTag('div', {className: 'rotate', title: 'повернути вправо'}, div);
      cntnr.appendChild(div);
      var reader = new FileReader();
      addEvent(reader, 'load', function(e) {
        var img = document.createElement('img');
        img.src = getTarget(e).result;
        addEvent(img, 'load', function(e) {
          loadImg(e, aId);
        });
      });
      reader.readAsDataURL(files[i]);
    })(i);
  });
  addEvent(cntnr, 'click', function(e) {
    var target = getTarget(e);
    if (target.className == 'remove') {
      cntnr.removeChild(target.parentNode);
      return;
    }
    if (target.className == 'rotate') {
      var a = target.parentNode.firstChild;
      var preview = a.firstChild;
      var image = new Image();
      image.src = a.href;
      addEvent(image, 'load', function(e) {
        rotateImg(getTarget(e), function(e) {
          a.href = getTarget(e).src;
        });
      });
      rotateImg(preview, a.id);
      preview.remove();
    }
  });
  addEvent(postButton, 'click', function(e) {
    e.preventDefault();
    var target = getTarget(e);
    var postForm = document.forms.photo_form;
    var files = cntnr.getElementsByClassName('photo');
    if (!files.length) {
      info.show('Додайте хоча б одне фото');
      return false;
    }
    for (var i=0; i<files.length; i++) {
      body += '&photo' + i + '=' + encodeURIComponent(files[i].firstChild.href + ',' + files[i].firstChild.firstChild.src);
    }
    var body = 'pic=' + encodeURIComponent(files.length);
    //file.upload(body, '/post.php?F=post&add=1', function() {
      info.show('Фото успішно опубліковано');
    //});
    blockForm(postForm, target);
    target.style.background = 'url(img/load.gif) center no-repeat';
    return false;
  });
  function loadImg(e, id) {
    var img = getTarget(e);
    var image = new Img(img);
    image.resize({max: 800}).rotateExif().append(function(e) {
      var targetImg = getTarget(e);
      document.getElementById(id).href = targetImg.src;
      var preview = new Img(targetImg);
      preview.resize({min: 150}).cropSquare().append(id);
      if (links.length == images.length) addPhotoStatus.busy(false);
    });
  }
  function rotateImg(img, param) {
    var rotateImg = new Img(img);
    rotateImg.rotate(90).append(param);
  }
}
};
