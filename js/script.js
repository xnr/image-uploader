/*
Вспомогательные функции и обьекты
*/
function addTag(tagName, attr, parent) {
  var tag = document.createElement(tagName);
  for (var name in attr) tag[name] = attr[name];
  if (parent) {
    parent.appendChild(tag);
    return;
  }
  document.body.appendChild(tag);
}
function addEvent(elem, type, handler) {
  elem.addEventListener ? elem.addEventListener(type, handler, false) : elem.attachEvent("on"+type, handler);
}
function removeEvent(elem, type, handler) {
  elem.removeEventListener ? elem.removeEventListener(type, handler, false) : elem.detachEvent("on"+type, handler);
}
function getTarget(e) {return e && e.target || event.srcElement}
(function() {
  if (!Element.prototype.remove) {
    Element.prototype.remove = function remove() {
      if (this.parentNode) {
        this.parentNode.removeChild(this);
      }
    };
  }
})();
Element.prototype.addClass = function(cls) {
  for(var c=this.className.split(' '), i=c.length-1; i>=0; i--) if (c[i] == cls) return;
  this.className == "" ? this.className = cls : this.className += ' ' + cls;
}
Element.prototype.remClass = function(cls) {
  for(var c=this.className.split(' '), i=c.length-1; i>=0; i--) if (c[i] == cls) c.splice(i,1);
  this.className = c.join(' ');
}
function imageSlider(mainId) {
  var number = 0,
      cover = document.createElement('DIV');
  cover.id = 'cover';
  document.getElementById(mainId).onclick = mainClick;
  function mainClick(e) {
    var target = getTarget(e);
    if (!(target.tagName == 'IMG' && target.parentNode.tagName == 'A')) return;
    fixscroll(true);
    var img = document.createElement('IMG'),
        post = target.parentNode,
        src = [];
    while (post.tagName != 'P') post = post.parentNode;
    var postimg = post.getElementsByTagName('IMG');
    for (var i=0; i<postimg.length; i++) {
      src.push(postimg[i].parentNode.href);
      if (postimg[i] == target) {
        number = i;
        img.number = i;
      }
    }
    cover.appendChild(img);
    document.body.appendChild(cover);
    img.onload = function() {
      imgLoad(img);
    }
    nextImg(img,src);
    cover.onclick = function(e) {
      coverClick(getTarget(e), src);
    }
    return false;
  }
  function imgLoad(img) {
    img.remClass('noeffect');
    img.style.marginTop = (document.documentElement.clientHeight - img.height) / 2 + 'px';
    img.addClass('nxt');
  }
  function coverClick(target, src) {
    if (target.id == 'cover') {
      removeCover();
      return;
    }
    if (target.tagName == 'IMG') {
      target.number++;
      if (target.number >= src.length) target.number = 0;
      target.number != number ? nextImg(target, src) : lastImg(target);
    }
  }
  function lastImg(img) {
    prevImg(img);
    setTimeout(
      function() {removeCover()},
      400
    );
  }
  function removeCover() {
    fixscroll(false);
    cover.innerHTML = '';
    document.body.removeChild(cover);
  }
  function nextImg(img, src) {
    var prev = img.cloneNode(false),
        left = img.getBoundingClientRect().left;
    prev.style.left = left + 'px';
    prev.id = 'prev';
    cover.appendChild(prev);
    prevImg(prev);
    img.addClass('noeffect');
    img.remClass('nxt');
    setTimeout(
      function() {img.src = src[img.number]},
      100
    );
  }
  function prevImg(img) {
    window.scrollBy(0, 0);
    img.remClass('nxt');
    img.addClass('prv');
    setTimeout(
      function() {cover.removeChild(img)},
      400
    );
  }
  function fixscroll(arg) {
    if (!arg) {
      window.onscroll = null;
      return;
    }
    var startYscroll = window.pageYOffset ? window.pageYOffset : document.documentElement.scrollTop;
    window.onscroll = function() {
      window.scrollTo(0, startYscroll);
    }
  }
}
function Info() {
  var parent = document.createElement("div");
  parent.className = "info_parent";
  addTag("div", {className: "info_message"}, parent);
  this.root = parent;
  this.message = parent.firstChild;
  this.showtime = 10000;
  this.timer = 0;
}
Info.prototype.show = function(message) {
  var self = this;
  this.message.innerHTML = message;
  if (this.timer != 0) {
    clearTimeout(this.timer);
    this.timer = setTimeout(
      function() {self.hide.call(self)},
      this.showtime
    );
    return;
  }
  document.body.appendChild(this.root);
  this.timer = setTimeout(
    function() {self.hide.call(self)},
    this.showtime
  );
}
Info.prototype.error = function(message) {
  this.show("<span class='info_error'>" + message + "</span>");
}
Info.prototype.hide = function() {
  this.timer = 0;
  document.body.removeChild(this.root);
}